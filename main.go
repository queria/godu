// go build . && /usr/bin/time -v ./godu
package main

/******
* Copyright © 2019, Queria Sa-Tas <public@sa-tas.net> All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
*
*   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*
*   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*
*   * Neither the name of the Queria Sa-Tas nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL QUERIA SA-TAS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING INCLUDING ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import (
    "flag"
    "fmt"
    "io/ioutil"
    "path/filepath"
    "os"
    // "os/exec"
    // "strconv"
    // "strings"
    // "sort"
    "runtime"
    "time"
)

var (
    arg_verbose bool = false
    arg_help bool = false
    arg_summary_only bool = false
)

func _err(e error) {
    if e != nil {
        fmt.Println("ERROR: ", e)
        os.Exit(1)
    }
}

func debug(msgs ...interface{}) {
    if arg_verbose {
        fmt.Fprintf(os.Stderr, "[debug] %v\n", msgs)
    }
}

func timeit(label string, started time.Time) {
    if arg_verbose {
        var mem runtime.MemStats
        runtime.ReadMemStats(&mem)
        fmt.Fprintln(os.Stderr, "[timeit]", time.Since(started), label, "\t// sysMem:", mem.Sys/1024/1024, "MB")
    }
}

func readlink_f(path string) string {
    path, err := filepath.EvalSymlinks(path)
    _err(err)
    path, err = filepath.Abs(path)
    _err(err)
    return path
}

func humanSize(size float64) string {
    suff := []string{"B", "KB", "MB", "GB", "TB", "PB"}
    cnt := 0

    for(size > 1024) {
        cnt++
        size = size / 1024
    }
    return fmt.Sprintf("%.2f%s", size, suff[cnt])
}

type PathSize struct {
    path string
    size int64
}

func totalSize_Walk(path string) (int64, []PathSize) {
    var size int64 = int64(0)
    var files []PathSize
    err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
        if(err != nil) {
            fmt.Println(err)
            return nil
        }
        if (arg_summary_only) {
            size += info.Size()
            return nil
        }
        ps := PathSize{path, info.Size()}
        size += ps.size
        files = append(files, ps)
        return nil
    })
    _err(err)
    return size, files
}

func totalSize_IOReadDir(path string) (int64, []PathSize) {
    var size int64 = int64(0)
    var dirs []string
    var dir string
    var allFiles []PathSize

    dirs = append(dirs, path)
    // there is bug/feature here - original "path" dir is missing in allFiles
    for (len(dirs) > 0) {
        dir, dirs = dirs[0], dirs[1:]
        files, err := ioutil.ReadDir(dir)
        _err(err)
        for _, f := range files {
            // fmt.Println(f.Name(), " ", f.IsDir())
            if (f.IsDir()) {
                dirs = append(dirs, filepath.Join(dir, f.Name()))
            }
            size += f.Size()
            if (! arg_summary_only) {
                ps := PathSize{filepath.Join(dir, f.Name()), f.Size()}
                allFiles = append(allFiles, ps)
            }
        }
    }

    return size, allFiles
}

func main() {
    defer timeit("main", time.Now())
    var start_dir string

    flag.BoolVar(&arg_verbose, "v", false, "Enable verbose/debug output")
    flag.BoolVar(&arg_help, "h", false, "Show help")
    flag.BoolVar(&arg_help, "help", false, "")
    flag.BoolVar(&arg_summary_only, "s", arg_summary_only, "Show only size summary (do not keep list of files, decreases mem usage a lot)")
    flag.BoolVar(&arg_summary_only, "summary", arg_summary_only, "") // flag.Lookup("s").Usage)
    flag.Parse()

    if (arg_help || flag.NArg() < 1) {
         fmt.Println("Usage:  ./godu [opts] <directory_to_scan>")
         fmt.Println("Options available:")
         flag.PrintDefaults()
         os.Exit(2)
    }
    start_dir = flag.Arg(0)
    start_dir = readlink_f(start_dir)
    fmt.Println("Going to scan path: ", start_dir)

    tmp_time := time.Now()

    var _s int64
    var _f []PathSize

    _s, _f = totalSize_IOReadDir(start_dir)
    fmt.Println("IOReadDir:", humanSize(float64(_s)), "in", len(_f), "files")
    timeit("IOReadDir", tmp_time)

    tmp_time = time.Now()
    _s, _f = totalSize_Walk(start_dir)
    fmt.Println("Walk:", humanSize(float64(_s)), "in", len(_f), "files")
    timeit("Walk", tmp_time)
}
